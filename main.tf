terraform {
  required_providers {
    proxmox = {
      source = "Telmate/proxmox"
      version = "2.7.4"
    }
    ansible = {
      source  = "nbering/ansible"
      version = "1.0.4"
    }

  }
  backend "http" {
	
}
}


provider "proxmox" {
                pm_parallel         = 1
                pm_tls_insecure     = true
                pm_api_url          = var.proxmox_url
                pm_api_token_id     = var.proxmox_api_token_id
                pm_api_token_secret = var.pmroxmox_api_token_secret
}



resource "proxmox_vm_qemu" "test-zone-node" {
	agent = 1
  desc = "TEST_ZONE_VMS"
	boot = "order=scsi0;ide2;net0"
	count = length(var.test_zone_nodes)
  name = var.test_zone_nodes[count.index]
	target_node = lookup(var.test_zone_node_profiles[var.test_zone_nodes[count.index]],"proxmox_node")
	clone = lookup(var.test_zone_node_profiles[var.test_zone_nodes[count.index]],"template_name")
	os_type = "cloud-init"
	cores = lookup(var.test_zone_node_profiles[var.test_zone_nodes[count.index]],"vcpu")
	sockets = 1
	memory = lookup(var.test_zone_node_profiles[var.test_zone_nodes[count.index]],"memory")
	full_clone = true

		disk {
			size = "10G"
			storage = lookup(var.test_zone_node_profiles[var.test_zone_nodes[count.index]],"storage")
			type = "scsi"
			format = "raw"
		     }



    		network {
        		model = "virtio"
        		bridge = "vmbr1"
			}

		 ipconfig0  = "ip=${lookup(var.test_zone_node_profiles[var.test_zone_nodes[count.index]],"ip")}/${var.test_zone_nodes_net["mask"]},gw=${var.test_zone_nodes_net["gw"]}"
		

		// ipconfig0  = "ip=${cidrhost(var.test_zone_nodes_net["test_zone_internal"],count.index)}/${var.test_zone_nodes_net["mask"]},gw=${var.test_zone_nodes_net["gw"]}"
		
lifecycle {
    ignore_changes  = [
      network,
    ]
  }
  
sshkeys = <<EOF
  ${var.ssh_key}
  EOF
}

resource "ansible_host" "all_nodes" {
	groups = ["all"]
	count = length(var.test_zone_nodes)
	inventory_hostname = var.test_zone_nodes[count.index]
	vars = {
    ansible_host = lookup(var.test_zone_node_profiles[var.test_zone_nodes[count.index]],"ip")
    	}

}
resource "ansible_host" "db_nodes" {
	groups = ["DB"]
	count = length(var.test_zone_nodes_db)
	inventory_hostname = var.test_zone_nodes_db[count.index]
	vars = {
    ansible_host = lookup(var.test_zone_node_profiles[var.test_zone_nodes[count.index]],"ip")
    	}

}
resource "ansible_host" "dcs_nodes" {
	groups = ["DCS"]
	count = length(var.test_zone_nodes_dcs)
	inventory_hostname = var.test_zone_nodes_dcs[count.index]
	vars = {
    ansible_host = lookup(var.test_zone_node_profiles[var.test_zone_nodes[count.index]],"ip")
    	}

}
resource "local_file" "ansible-inventory" {
  content = templatefile("${path.module}/templates/inventory.tpl",{
    ansible_hosts_all = ansible_host.all_nodes.*
    ansible_hosts_db = ansible_host.db_nodes.*
    ansible_hosts_dcs = ansible_host.dcs_nodes.*
    })
  filename = "hosts"
}




