variable "proxmox_url" {
 type = string
 default = "https://you-uri:8006/api2/json"
}

variable "proxmox_api_token_id" {
 type = string
 default = "root@pam!terraformkey"
}

variable "pmroxmox_api_token_secret" {
 type = string
 default = ""
}

// Deploy Pub-key
variable "ssh_key" {
 type = string
 default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQChuZkoAa8Q2+1QBR2VtSJzwKdKsf5dr/fAAUKfcnKaayCW1IlWTuhlTuZtexqeveU7w6+OKynI8BvflGZPiatEZT+gsBSXS708LG/IWDh0cZcYEZjslDBcNBwaHvjr0gxOU2V3XyMjMkE1qAKsX57ySaoTxbXIFhaOMQPQcOteTdfys5jJxZ7XmAacNxYIwWohRMLNyUYqCXA11tygWNOKhrIq/ptL3AeIycYFI1A7z47NDE4At56yOemc6fFixmzJ8Ep16Z3ZJ0yMRafAd3zmX8UV4fe8WtVB/W/t41iO4MdrZFQvwI2CLc43Ndbe1s5hpmpvbizLkqnPz38yO5Vl/MR8EHFl99+rgjDtssdonIl2BQayzsUJT8qvmSd8L24Eu50p6fDJxgzZPJ4xx0Y4oN8Q7cAhV9kE9oRHOEt1UVvSlDeSXf1QsMIM3kB7w5qLdttZ6C7sn36bhR87Quv+Uv2QZC7K3GxESNU5h1dlWkUiK68lqWj+VYI1eLlBeyL4LUFK1Z+LTNlGsxyzBKULbMHiPSTd/N+a8IsNz5dE2wzlwKZxXO6eCQ6GitDDS8jnOP+MTna/Jf1Yj/cCKz0U5eGQUEybeAp/ZqsY5fFvrF2lSXqe4aZu//vNcqeJ+pBanDIzCd8gZHhiA4ggmYYaN89WhyqXBwOvCASO4JOInw== root@terraform"
}


// TEST  NODES


variable "test_zone_nodes" {
    type = list(string)
    default = [
        "test-patroni1",
        "test-patroni2",
        "test-dcs1",
        "test-dcs2",
        "test-dcs3"]
}

variable "test_zone_nodes_db" {
    type = list(string)
    default = [
        "test-patroni1",
        "test-patroni2"]
}
variable "test_zone_nodes_dcs" {
    type = list(string)
    default = [
        "test-dcs1",
        "test-dcs2",
        "test-dcs3"]
}
// Resources

variable "test_zone_node_profiles" {
        type = map
        default = {
                "test-patroni1" = {
                        "proxmox_node" = "alpha"
                        "storage" = "local"
                        "template_name" = "Centos7"
                        "vcpu" = 2
                        "memory" = 2048
                        "ip" = "192.168.11.130"
                },
  		"test-patroni2" = {
                        "proxmox_node" = "alpha"
                        "storage" = "local"
                        "template_name" = "Centos7"
                        "vcpu" = 2
                        "memory" = 2048
                        "ip" = "192.168.11.131"
                 },           
  		"test-dcs1" = {
                        "proxmox_node" = "alpha"
                        "storage" = "local"
                        "template_name" = "Centos7"
                        "vcpu" = 2
                        "memory" = 1024
                        "ip" = "192.168.11.132"
                 },
  		"test-dcs2" = {
                        "proxmox_node" = "alpha"
                        "storage" = "local"
                        "template_name" = "Centos7"
                        "vcpu" = 2
                        "memory" = 1024
                        "ip" = "192.168.11.133"
                 },
  		"test-dcs3" = {
                        "proxmox_node" = "alpha"
                        "storage" = "local"
                        "template_name" = "Centos7"
                        "vcpu" = 2
                        "memory" = 1024
                        "ip" = "192.168.11.134"
                 }
                                                                          
        }
}



variable "test_zone_nodes_net" {
	type = map
	default = {
		// "test_zone_internal" = "192.168.11.248/29"
		"mask" = 24
                "gw" = "192.168.11.10"
		}
}

